<?php

namespace designerei\ContaoToplinkBundle\Controller\FrontendModule;

use Contao\ModuleModel;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Template;
use Contao\FilesModel;
use Contao\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FrontendModule("toplink",
 *   category="miscellaneous"
 * )
 */
class ToplinkController extends AbstractFrontendModuleController
{
    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        // adjust naming of some variables
        $template->addIcon = $model->toplinkAddIcon;
        $template->text = $model->toplinkText;

        // add class
        $template->class = $template->class . " toplink";

        // add id
        $template->cssID = $template->cssID . "id='toplink'";

        return $template->getResponse();
    }
}
