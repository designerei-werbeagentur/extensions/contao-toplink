<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['toplink'] =
    '{title_legend},name,type;'
    . '{toplink_legend},toplinkText, toplinkAddIcon;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID'
;

$GLOBALS['TL_DCA']['tl_module']['fields']['toplinkText'] = array
(
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default 'Top'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['toplinkAddIcon'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true, 'tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['config']['onload_callback'][] = function ($dc) {
  $objFmd = Contao\ModuleModel::findByPk($dc->id);
  if ($objFmd->type === 'toplink') {
    \Contao\Message::addInfo(sprintf($GLOBALS['TL_LANG']['tl_module']['includeTemplate'], 'j_toplink'));
  }
};
